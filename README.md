Generative Models: Boltzmann Machines (e.g. Restricted Boltzmann Machine - RBM, Deep Belief Network - DBN), Generative Adversarial Networks (e.g. Deep Convolutional GAN - DCGAN), Autoencoders (e.g. Variational Autoencoders - VAE). Implemented in Python using NumPy, PyTorch and Matplotlib library.

Code and task description in corresponding Jupyter Notebooks. Experiments conducted on Google Colab.


Notebook 1: BoltzmannMachines

1 Restricted Boltzmann Machine

2 Deep Belief Network

3 Deep belief network with generative fine tuning


Notebook 2: VariationalAutoencoders

4 Variational Autoencoders


Notebook 3: GenerativeAdversarialNetworks

5 Deep Convolutional GAN (DCGAN) 


My lab assignment in Deep Learning, FER, Zagreb.

Created: 2021

